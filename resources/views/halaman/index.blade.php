@extends('layout.master')

@section('judul')
Media Online    
@endsection

@section('content')

<h3>Sosial Media Developer</h3>
<h5>Belajar dan berbagi agar hidup menjadi lebih baik</h5>
<h4>Benefit Join di Media Online</h4> 

<ul>
    <li>Mendapatkan motivasi dari sesama para Developer</li>
    <li>Sharing knowledge</li>
    <li>Dibuat oleh calon web developer terbaik</li>
</ul>

<h4>Cara Bergabung ke Media Online</h4>

<ol>
    <li>Mengunjungi Website Ini</li>
    <li>Mendaftarkan di <a href="/register">Form Sign Up</a>  </li>
    <li>Selesai</li>        
</ol> 
@endsection
    
