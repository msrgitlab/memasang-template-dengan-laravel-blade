@extends('layout.master')

@section('judul')
Selamat Datang!    
@endsection

@section('content')
    <h1>Selamat Datang! {{$nama}} {{$nama2}}</h1>
    <p>Terima kasih telah bergabung di Website Kami. Media Belajar kita semua!</p>
@endsection